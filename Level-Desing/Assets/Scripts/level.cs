﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class level : MonoBehaviour {

    public int num;
    public Transform playerStartingPoint;
    private Transform[] childs;

    void Start() {
        childs = GetComponentsInChildren<Transform>();
    }
    void Update() {

    }

    public int GetNum()
    {
        return num;
    }


    public void RestartLevel()
    {
        Debug.Log(childs.Length);
        for (int i = 0; i < childs.Length; i++)
        {
            GameObject gObj = childs[i].gameObject;

            Debug.Log(gObj.name);
            if (!gObj.activeInHierarchy)
            {
                Debug.Log(gObj.name);
                gObj.SetActive(true);
            }

            
            if (gObj.name == "DoorSwitch")
                gObj.GetComponent<DoorSwitch>().Restart();
        }
    }
}
