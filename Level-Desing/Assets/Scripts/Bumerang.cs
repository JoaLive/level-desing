﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bumerang : MonoBehaviour {

    private IEnemyStrategy strategy;
    private BumerangGo goStrat;
    private BumerangBack backStrat;
    private Transform playerBumPos;
    private Transform player;
    private Rigidbody2D rigidB;
    private Animator anim;
    private SpriteRenderer sprite;

    private bool isFlying;
    private bool isStuck;
    private bool isReturning;

    private bool inPossesion;

    public float gravityForce;

    private GameObject playerCheck;
    private GameObject wallCheck;
	
	void Start () {
        goStrat = GetComponent <BumerangGo>();
        backStrat = GetComponent<BumerangBack>();
        strategy = backStrat;
        player = GameObject.FindGameObjectWithTag("Player").transform;
        playerBumPos = GameObject.FindGameObjectWithTag("BumerangPos").transform;
        playerCheck = GameObject.Find("BumerangPlayerCheck");
        wallCheck = GameObject.Find("BumerangWallCheck");
        rigidB = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        sprite = GetComponent<SpriteRenderer>();
        inPossesion = true;
        isReturning = false;
	}

    private void FixedUpdate() {

        if (!isFlying)
        {
            if (player.position.x < transform.position.x)
                sprite.flipX = true;
            else
                sprite.flipX = false;
        }

        if (transform.position == player.position)
            Returned();

        InputManager();
        if (strategy != null)
            strategy.Execute();

        playerCheck.transform.position = transform.position;
        wallCheck.transform.position = transform.position;


    }

    private void InputManager()
    {
        if (inPossesion && Input.GetKeyDown(KeyCode.Z))
        {
            transform.position = playerBumPos.position;
            playerCheck.SetActive(false);
            wallCheck.SetActive(true);
            strategy = goStrat;
            isFlying = true;
            inPossesion = false;
        }

        else if (!inPossesion && Input.GetKeyDown(KeyCode.Z))
        {
            rigidB.gravityScale = 0;
            playerCheck.SetActive(true);
            isStuck = false;
            isFlying = false;
            strategy = backStrat;
            isReturning = true;
            rigidB.gravityScale = 0;
            anim.Play("BumerangAtk");
        }

        else if (!inPossesion && Input.GetKeyDown(KeyCode.C))
        {
            playerCheck.SetActive(true);
            isStuck = false;
            isFlying = false;
            strategy = backStrat;
            isReturning = false;
            inPossesion = true;
            rigidB.gravityScale = 0;
            transform.position = playerBumPos.position;
            Returned();
        }

        else if (isStuck && Input.GetKeyDown(KeyCode.X))
        {
            rigidB.gravityScale = gravityForce;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "DamageObject" && isReturning)
            other.gameObject.SetActive(false);

        else if (other.tag == "WeakWall")
            other.gameObject.SetActive(false);
    }

    public void HitWall()
    {
        if (!inPossesion)
        {
            playerCheck.SetActive(true);
            isStuck = true;
            isFlying = false;
            strategy = null;
            rigidB.gravityScale = 0;
        }

    }

    public void Returned()
    {
        anim.Play("BumerangIdle");
        strategy = backStrat;
        inPossesion = true;
        isReturning = false;
        isStuck = false;
        wallCheck.SetActive(false);
        playerCheck.SetActive(false);
    }

    public bool IsInPossesion()
    {
        return inPossesion;
    }
}
