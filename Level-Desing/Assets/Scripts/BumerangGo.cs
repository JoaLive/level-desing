﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BumerangGo : MonoBehaviour, IEnemyStrategy {

    [SerializeField]
    private float speed;
    private Transform playerT;

	void Start () {
        playerT = GameObject.FindGameObjectWithTag("Player").transform;
	}

    public void Execute()
    {
        transform.Translate(playerT.right  * speed * Time.deltaTime);
    }
}
