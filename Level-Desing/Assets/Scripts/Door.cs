﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour {

    private BoxCollider2D wall;
    private Animator anim;

	void Start () {
        wall = GetComponentInChildren<BoxCollider2D>();
        anim = GetComponent<Animator>();
	}
	
	void Update () {
		
	}

    public void RaiseDoor()
    {
        anim.Play("DoorDeact");
        wall.gameObject.SetActive(false);
    }

    public void Restart()
    {
        anim.Play("DoorIdle");
        wall.gameObject.SetActive(false);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Bumerang")
        {
            collision.gameObject.GetComponentInParent<Bumerang>().HitWall();
        }
    }
}
