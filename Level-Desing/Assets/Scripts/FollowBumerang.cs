﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowBumerang : MonoBehaviour {

    private Transform bumerang;

	void Start () {
        bumerang = GameObject.FindGameObjectWithTag("Bumerang").transform;
	}
	
	void Update () {
        transform.position = bumerang.position;
	}
}
