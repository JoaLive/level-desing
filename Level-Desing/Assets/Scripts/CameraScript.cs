﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {

    private Transform player;
    private Transform bumerang;
    private GameObject[] rawLevels;
    private List<level> levels;
    private int levelActual;
    private bool needToMove;
    void Start() {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        bumerang = GameObject.FindGameObjectWithTag("Bumerang").transform;
        rawLevels = GameObject.FindGameObjectsWithTag("level");
        levels = new List<level>();

        int i = 0;
        int lvl = 1;
        while (levels.Count < rawLevels.Length)
        {
            level auxLvl = rawLevels[i].GetComponent<level>(); 
            if (auxLvl.GetNum() == lvl)
            {
                levels.Add(auxLvl);
                i = 0;
                lvl++;
                continue;
            }
            i++;
        }

        levelActual = 1;
        needToMove = false;
	}
	
	void Update () {
        if (needToMove)
            MoveToNextLevel();      
	}

    public void LevelFinished()
    {
        levelActual++;
        if (levelActual == 11)
            Application.LoadLevel("Start");
        else
            needToMove = true;
    }
    
    public void MoveToNextLevel()
    {
        transform.position = levels[levelActual-1].transform.position;
        transform.position = new Vector3(transform.position.x, transform.position.y, -10);
        player.transform.position = levels[levelActual - 1].playerStartingPoint.position;
        bumerang.transform.position = levels[levelActual - 1].playerStartingPoint.position;
        needToMove = false;
    }

    public Vector3 GetActiveLvlPlayerPos()
    {
        return levels[levelActual - 1].playerStartingPoint.position;
    }

    public void RestartLevel()
    {
        levels[levelActual-1].RestartLevel();
    }
}
