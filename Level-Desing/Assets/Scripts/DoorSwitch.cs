﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorSwitch : MonoBehaviour {

    private Door door;
    private Animator anim;

	void Start () {
        door = GetComponentInChildren<Door>();
        anim = GetComponent<Animator>();
	}
	
	void Update () {
		
	}

    public void Restart()
    {
        anim.Play("SwitchIdle");
        door.Restart();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Bumerang")
        {
            if (!other.GetComponent<Bumerang>().IsInPossesion())
            {
                anim.Play("SwitchDed");
                door.RaiseDoor();
            }
        }
    }
}
