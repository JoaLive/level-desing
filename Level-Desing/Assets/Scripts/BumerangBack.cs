﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BumerangBack : MonoBehaviour, IEnemyStrategy {

    private Transform player;
    [SerializeField]
    private float speed;
    public float offset;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("BumerangPos").transform;
    }

    public void Execute()
    {
        Vector2 fixedOffset = new Vector2((offset * -player.right.x) + player.position.x, player.position.y);
        transform.position = Vector2.MoveTowards(transform.position, fixedOffset, speed * Time.deltaTime);
    }
}
